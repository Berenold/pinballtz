﻿using UnityEngine;
using UnityEngine.UI;

public class InfoPanelController: MonoBehaviour, IInfoPanel
{
  public Text scoreText;
  public Text ballLeftText;

  private bool _error;

  public void InitPanel()
  {
    CheckObjects();

    if(!_error)
    {
      ReloadValue();
    }
  }

  public void ReloadValue()
  {
    scoreText.text = ApplicationContainer.Instance.playerManager.score.ToString("D8");
    ballLeftText.text = ApplicationContainer.Instance.playerManager.ballLeft.ToString();
  }

  private void CheckObjects()
  {
    if(scoreText == null)
    {
      Debug.LogError(string.Format("[{0}][CheckObjects]scoreText is null!", GetType().Name));
      _error = true;
    }

    if(ballLeftText == null)
    {
      Debug.LogError(string.Format("[{0}][CheckObjects]ballLeftText is null!", GetType().Name));
      _error = true;
    }
  }
}