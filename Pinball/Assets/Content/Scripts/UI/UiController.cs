﻿using UnityEngine;

public class UiController: MonoBehaviour
{
  public StartGamePanelController startGamePanel;
  public InfoPanelController infoPanel;
  public RestartGamePanelController restartGamePanel;


  public void InitPanel()
  {
    startGamePanel.gameObject.SetActive(true);
  }

  public void StartGame(StartGameParam param)
  {
    startGamePanel.gameObject.SetActive(false);
    infoPanel.InitPanel();
    infoPanel.gameObject.SetActive(true);
    ApplicationContainer.Instance.StartGame(param);
  }

  public void GameOver()
  {
    infoPanel.gameObject.SetActive(false);
    restartGamePanel.InitPanel();
    restartGamePanel.gameObject.SetActive(true);
  }

  public void ReloadGame()
  {
    restartGamePanel.gameObject.SetActive(false);
    startGamePanel.gameObject.SetActive(true);
  }

  private void OnEnable()
  {
    EventHolder.Instance.addNewBallEvent += ReloadInfo;
  }

  public void ReloadInfo()
  {
    infoPanel.ReloadValue();
  }
}