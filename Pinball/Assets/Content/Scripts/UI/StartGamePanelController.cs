﻿using UnityEngine;

public class StartGamePanelController: MonoBehaviour
{
  public void StartGamer()
  {
    ApplicationContainer.Instance.uiController.StartGame(StartGameParam.Player);
  }

  public void StartGamerAI()
  {
    ApplicationContainer.Instance.uiController.StartGame(StartGameParam.AI);
  }
}