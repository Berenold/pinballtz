﻿using UnityEngine;
using UnityEngine.UI;

public class RestartGamePanelController: MonoBehaviour, IInfoPanel
{
  public Text scoreText;
  private bool _error;

  public void InitPanel()
  {
    CheckObjects();

    if(!_error)
    {
      ReloadValue();
    }
  }

  public void DoneButton()
  {
    ApplicationContainer.Instance.uiController.ReloadGame();
  }

  public void ReloadValue()
  {
    scoreText.text = ApplicationContainer.Instance.playerManager.score.ToString("D8");
  }

  private void CheckObjects()
  {
    if(scoreText == null)
    {
      Debug.LogError(string.Format("[{0}][CheckObjects]scoreText is null!", GetType().Name));
      _error = true;
    }
  }
}