﻿using UnityEngine;

public class ConfigHolder: MonoBehaviour
{
  public float triangleBumperForce;
  public float bumperForce;
  public float bumperForceRadius;
  public float handleForce;
  public Vector3 handleForceDirection;
  public int bumperScore;
  public int triangleBumperScore;
  public int dropObjectScore;
  public int ballForLevel;
}

public class LoadedConfig
{
  public float triangleBumperForce;
  public float bumperForce;
  public float bumperForceRadius;
  public float handleForce;
  public Vector3 handleForceDirection;
  public int bumperScore;
  public int triangleBumperScore;
  public int dropObjectScore;
  public int ballForLevel;
}