﻿using UnityEngine;
using System;

public class EventHolder: Singleton<EventHolder>
{
  public Action initValuesEvent;
  public void InitValues()
  {
    if(initValuesEvent != null)
    {
      initValuesEvent();
    }
  }

  public Action addNewBallEvent;
  public void AddNewBall()
  {
    if(addNewBallEvent != null)
    {
      addNewBallEvent();
    }
  }

  public Action resetDropTargetObjectsEvent;
  public void ResetDropTargetObjects()
  {
    if(resetDropTargetObjectsEvent != null)
    {
      resetDropTargetObjectsEvent();
    }
  }

  public Action<ControllPosition> aiHandleActionEvent;
  public void AiHandleAction(ControllPosition position)
  {
    if(aiHandleActionEvent != null)
    {
      aiHandleActionEvent(position);
    }
  }

  public Action<float> aiCickBallEvent;
  public void AiCickBall(float percent)
  {
    aiCickBallEvent(percent);
  }
}