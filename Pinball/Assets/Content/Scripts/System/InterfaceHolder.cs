﻿public interface IInfoPanel
{
  void InitPanel();
  void ReloadValue();
}
