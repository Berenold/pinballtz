﻿using UnityEngine;
using System;
using System.Collections;

public class FileManager: MonoBehaviour 
{
  public void LoadConfigs(Action<string> data)
  {
    WWW www = new WWW(GetUrl());
    StartCoroutine("LoadConfigsCoroutine", new CoroutineParamsWithData(www, data));
  }

  private IEnumerator LoadConfigsCoroutine(CoroutineParamsWithData data)
  {
    yield return data.www;

    if(string.IsNullOrEmpty(data.www.error))
    {
      data.data.Invoke(data.www.text);
    }
    else
    {
      Debug.LogError(string.Format("[{0}][LoadConfigsCoroutine]WWW Error: {1}!", GetType().Name, data.www.error));
      data.data.Invoke("");
    }

    data.www.Dispose();
  }

  private string GetUrl()
  {
    string returnValue = string.Empty;

#if UNITY_IOS
    returnValue = string.Format("{0}/Raw/game.config", Application.dataPath);
#endif
#if UNITY_ANDROID
    returnValue = string.Format("jar:file://{0}!/assets/game.config", Application.dataPath);
    #endif
    #if UNITY_EDITOR
    returnValue = string.Format("file://{0}/StreamingAssets/game.config", Application.dataPath);
    #endif
    return returnValue;
  }

  private class CoroutineParamsWithData
  {
    public WWW www;
    public Action<string> data;

    public CoroutineParamsWithData(WWW www, Action<string> data)
    {
      this.data = data;
      this.www = www;
    }
  }
}