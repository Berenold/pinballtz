﻿using UnityEngine;

public class TriangleBumperController: MonoBehaviour
{
  public float _force;

  private void OnEnable()
  {
    EventHolder.Instance.initValuesEvent += InitValues;
  }

  private void InitValues()
  {
    _force = ConfigHolderValue().triangleBumperForce;
  }

  private ConfigHolder ConfigHolderValue()
  {
    return ApplicationContainer.Instance.configHolder;
  }

  private void OnTriggerEnter(Collider colliderObject)
  {
    if(colliderObject.attachedRigidbody)
    {
      colliderObject.attachedRigidbody.AddForce(transform.forward*_force);
      ApplicationContainer.Instance.playerManager.AddScore(ScoreObjects.TriangleBumper);
    }
  }
}
