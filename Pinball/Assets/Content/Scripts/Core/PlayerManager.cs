﻿using UnityEngine;
using System.Collections;

public class PlayerManager: MonoBehaviour
{
  public int ballLeft;
  public int score;

  public void InitValues()
  {
    ballLeft = ConfigHolderValue().ballForLevel;
    score = 0;
  }

  public void AddScore(ScoreObjects scoreObjects)
  {
    switch(scoreObjects)
    {
      case ScoreObjects.Bumper:
        score += ConfigHolderValue().bumperScore;
        break;
      case ScoreObjects.TriangleBumper:
        score += ConfigHolderValue().triangleBumperScore;
        break;
      case ScoreObjects.DropObject:
        score += ConfigHolderValue().dropObjectScore;
        break;
    }

    ApplicationContainer.Instance.uiController.ReloadInfo();
  }

  private ConfigHolder ConfigHolderValue()
  {
    return ApplicationContainer.Instance.configHolder;
  }

  public void DecrementBall()
  {
    StartCoroutine("NextStep");
  }

  private IEnumerator NextStep()
  {
    yield return new WaitForSeconds(2);

    if(ballLeft > 0)
    {
      ballLeft--;
      EventHolder.Instance.AddNewBall();
      StartCoroutine("AiCickBallCoroutine");
    }
    else
    {
      ApplicationContainer.Instance.GameOver();
    }
  }

  private IEnumerator AiCickBallCoroutine()
  {
    yield return new WaitForSeconds(1);
    EventHolder.Instance.AiCickBall(Random.Range(1, 101));
  }
}

public enum ScoreObjects
{
  Bumper,
  TriangleBumper,
  DropObject,
  Default
}