﻿using UnityEngine;

public class DeadZoneController: MonoBehaviour
{
  private void OnTriggerEnter(Collider colliderObject)
  {
    if(colliderObject.attachedRigidbody)
    {
      Destroy(colliderObject.gameObject);
      ApplicationContainer.Instance.playerManager.DecrementBall();
    }
  }
}
