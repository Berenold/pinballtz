﻿using UnityEngine;

public class GameFieldController: MonoBehaviour
{
  public GameObject ballDropZone;
  public GameObject ballPrefab;

  private bool _error;

  private void OnEnable()
  {
    EventHolder.Instance.addNewBallEvent += AddBall;
  }

  private void AddBall()
  {
    CheckObjects();
    if(!_error)
    {
      Instantiate(ballPrefab, ballDropZone.transform.position, new Quaternion());

    }
  }

  private void CheckObjects()
  {
    if(ballPrefab == null)
    {
      _error = true;
      Debug.LogError(string.Format("[{0}][CheckObjects]ballPrefab is null!",GetType().Name));
    }

    if(ballDropZone == null)
    { 
      _error = true;
      Debug.LogError(string.Format("[{0}][CheckObjects]ballDropZone is null!", GetType().Name));
    }
  }
}
