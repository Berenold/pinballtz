﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DropTargetBlockController: MonoBehaviour
{
  public DropTargetController[] dropTargets;
  private List<int> _droppedIndexList = new List<int>();

  private void OnEnable()
  {
    EventHolder.Instance.resetDropTargetObjectsEvent += ResetDropTargetObjects;
  }

  public void ResetDropTargetObjects()
  {
    if(_droppedIndexList.Count > 0)
    {
      for(int i = 0; i < _droppedIndexList.Count; i++)
      {
        dropTargets[_droppedIndexList[i]].RestorDropTarget();
      }

      _droppedIndexList = new List<int>();
    }
  }

  public void TargetIsDropped(int index)
  {
    if(!_droppedIndexList.Exists(element => element == index))
    {
      _droppedIndexList.Add(index);

      if(_droppedIndexList.Count == dropTargets.Length)
      {
        StartCoroutine("RestorAllDroppedTargets");
      }
    }
  }

  private IEnumerator RestorAllDroppedTargets()
  {
    ApplicationContainer.Instance.playerManager.AddScore(ScoreObjects.DropObject);
    yield return new WaitForSeconds(5);

    for(int i = 0; i < dropTargets.Length; i++)
    {
      dropTargets[i].RestorDropTarget();
    }

    _droppedIndexList = new List<int>();
  }
}
