﻿using UnityEngine;
using System.Collections;

public class DropTargetController: MonoBehaviour
{
  public int id;
  public float dropDistance = 2.0f;
  private bool isDropped;

  public void RestorDropTarget()
  {
    if(isDropped)
    {
      transform.position += Vector3.up * dropDistance;
      isDropped = false;
    }
  }

  private void OnCollisionEnter()
  {
    if(!isDropped)
    {
      transform.position += Vector3.down * dropDistance;
      isDropped = true;
      DropTargetBlockController block = transform.parent.GetComponent<DropTargetBlockController>();

      if(block != null)
      {
        block.TargetIsDropped(id);
      }
      else
      {
        Debug.LogError(string.Format("[{0}][OnCollisionEnter]block is null!", GetType().Name));
      }
    }
  }
}