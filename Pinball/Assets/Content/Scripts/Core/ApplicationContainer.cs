﻿using UnityEngine;
using System;

public class ApplicationContainer: Singleton<ApplicationContainer>
{
  public bool showFPS;
  public GameObject UiPrefab;
  public FpsView fpsView;
  public ConfigHolder configHolder;
  public PlayerManager playerManager;
  public UiController uiController;
  public FileManager fileManager;
  public bool aiControll;

  private  bool _isGameOver = false;

  public bool IsGameOver
  {
    get { return _isGameOver; }
  }

  private void Start()
  {
    InitSceneObjects();
    Action<string> action = LoadConfigsCallback;
    fileManager.LoadConfigs(action);
  }

  private void InitSceneObjects()
  {
    if(showFPS)
    {
      fpsView = new GameObject("FpsView").AddComponent<FpsView>();
    }

    configHolder = new GameObject("ConfigHolder").AddComponent<ConfigHolder>();
    playerManager = new GameObject("PlayerManager").AddComponent<PlayerManager>();
    fileManager = new GameObject("FileManager").AddComponent<FileManager>();

    if(UiPrefab != null)
    {
      uiController = Instantiate(UiPrefab).GetComponent<UiController>();
    }
  }

  private void LoadConfigsCallback(string data)
  {
    if(!string.IsNullOrEmpty(data))
    {
      LoadedConfig loadedData = JsonUtility.FromJson<LoadedConfig>(data);
      configHolder.bumperForce = loadedData.bumperForce;
      configHolder.handleForce = loadedData.handleForce;
      configHolder.bumperScore = loadedData.bumperScore;
      configHolder.ballForLevel = loadedData.ballForLevel;
      configHolder.dropObjectScore = loadedData.dropObjectScore;
      configHolder.bumperForceRadius = loadedData.bumperForceRadius;
      configHolder.triangleBumperForce = loadedData.triangleBumperForce;
      configHolder.triangleBumperScore = loadedData.triangleBumperScore;
      configHolder.handleForceDirection = loadedData.handleForceDirection;
      EventHolder.Instance.InitValues();
      uiController.InitPanel();
    }
  }

  public void StartGame(StartGameParam param)
  {
    /*switch(param)
    {
      case StartGameParam.Player:
        EventHolder.Instance.ResetDropTargetObjects();
        playerManager.InitValues();
        playerManager.DecrementBall();
        break;
      case StartGameParam.AI:
        aiControll = true;
        break;
    }*/

    if(param == StartGameParam.AI)
    {
      aiControll = true;
    }
    EventHolder.Instance.ResetDropTargetObjects();
    playerManager.InitValues();
    playerManager.DecrementBall();
  }

  public void GameOver()
  {
    if(aiControll)
    {
      aiControll = false;
    }

    _isGameOver = true;
    uiController.GameOver();
  }
}

public enum StartGameParam
{
  Player,
  AI
}