﻿using UnityEngine;

public class BumperController: MonoBehaviour
{
  private float _force = 100.0f;
  private float _forceRadius = 1.0f;

  private void OnEnable()
  {
    EventHolder.Instance.initValuesEvent += InitValues;
  }

  private void InitValues()
  {
    _force = ConfigHolderValue().bumperForce;
    _forceRadius = ConfigHolderValue().bumperForceRadius;
  }

  private ConfigHolder ConfigHolderValue()
  {
    return ApplicationContainer.Instance.configHolder;
  }

  private void OnCollisionEnter()
  {
    foreach(Collider collider in Physics.OverlapSphere(transform.position, _forceRadius))
    {
      if(collider.attachedRigidbody)
      {
        collider.attachedRigidbody.AddExplosionForce(_force, transform.position, _forceRadius);
        ApplicationContainer.Instance.playerManager.AddScore(ScoreObjects.Bumper);
      }
    }
  }
}