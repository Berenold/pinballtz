﻿using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class HandleController: MonoBehaviour
{
  public ControllPosition handlePosition;
  public Vector3 offset;

  private bool _isInit;
  private float _force;
  private Vector3 _forceDirection;
  private Rigidbody handleRigidbody;

  private void Start()
  {
    handleRigidbody = GetComponent<Rigidbody>();
  }

  private void Update()
  {
    if(_isInit)
    {
      HandleControllerLogick();
    }
  }

  private void HandleControllerLogick()
  {
    #if UNITY_ANDROID || UNITY_IOS
    if(Input.touchCount > 0)
    {
      for(int i = 0; i < Input.touchCount; i++)
      {
        if(Input.GetTouch(i).position.x < Screen.width / 2)
        {
          ActionLeftHandle();
        }
        else
        {
          ActionRightHandle();
        }
      }
    }
    #endif
    #if UNITY_EDITOR 
    if(Input.GetButton("LeftButton"))
    {
      ActionLeftHandle();
    }

    if(Input.GetButton("RightButton"))
    {
      ActionRightHandle();
    }
    #endif
  }

  private void OnEnable()
  {
    EventHolder.Instance.initValuesEvent += InitValues;
    EventHolder.Instance.aiHandleActionEvent += AiHandleAction;
  }

  private void ActionLeftHandle()
  {
    if(handlePosition == ControllPosition.Left)
    {
      if(!ApplicationContainer.Instance.aiControll)
      {
        HandleAddForce();
      }
    }
  }

  private void ActionRightHandle()
  {
    if(handlePosition == ControllPosition.Right)
    {
      if(!ApplicationContainer.Instance.aiControll)
      {
        HandleAddForce();
      }
    }
  }

  private void InitValues()
  {
    _isInit = true;
    _force = ConfigHolderValue().handleForce;
    _forceDirection = ConfigHolderValue().handleForceDirection;
  }

  private ConfigHolder ConfigHolderValue()
  {
    return ApplicationContainer.Instance.configHolder;
  }

  private void AiHandleAction(ControllPosition controllPosition)
  {
    if(handlePosition == controllPosition)
    {
      HandleAddForce();
    }
  }

  private void HandleAddForce()
  {
    handleRigidbody.AddForceAtPosition(_forceDirection.normalized * _force, transform.position + offset);
  }
}

public enum ControllPosition
{
  Left,
  Right,
  Default
}