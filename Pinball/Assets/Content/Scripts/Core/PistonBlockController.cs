﻿using UnityEngine;

public class PistonBlockController: MonoBehaviour
{
  public float minShiftDistance;
  public float maxShiftDistance;
  public PistonTriggerController pistonTrigger;


  private float _pistonDistance;
  private bool _movePiston = false;
  private Transform _pistone;
  private Vector3 _startPosition = Vector3.zero;
  private Vector3 _touchStartPosition = Vector3.zero;

  private void Start()
  {
    _startPosition = transform.position;
  }

  void Update()
  {
    PistonBlockLogick();
  }

  private void PistonBlockLogick()
  {
    if(!ApplicationContainer.Instance.aiControll)
    {
      #if UNITY_ANDROID || UNITY_IOS
      if(Application.platform == RuntimePlatform.Android) // Cделано для того, что бы отсечь данный код в эдиторе при выбранной платформе андроид
      {
        if(Input.touchCount > 0)
        {
          if(Input.GetTouch(0).phase == TouchPhase.Began)
          {
            CheckTouch(Input.GetTouch(0).position);
          }
        }
        else
        {
          if(_movePiston)
          {
            _movePiston = false;
            DoPower();
          }
        }
      }
      #endif
      #if UNITY_EDITOR
      if(Input.GetMouseButtonDown(0))
      {
        CheckTouch(Input.mousePosition);
      }
      if(Input.GetMouseButtonUp(0))
      {
        if(_movePiston)
        {
          _movePiston = false;
          DoPower();
        }
      }
      #endif
      if(_movePiston)
      {
        MovePistone();
      }
    }
  }

  private void CheckTouch(Vector2 position)
  {
    Ray ray = Camera.main.ScreenPointToRay(position);
    RaycastHit hit;

    if(Physics.Raycast(ray, out hit))
    {
      _pistone = hit.transform;
      _touchStartPosition = Camera.main.ScreenToWorldPoint(new Vector3(position.x, position.y, Camera.main.transform.position.y));
      _startPosition = _pistone.position;
      _movePiston = true;
    }
  }

  private void MovePistone()
  {
    Vector3 touchPosition = Vector3.zero;
    #if UNITY_ANDROID || UNITY_IOS
    if(Application.platform == RuntimePlatform.Android) // Cделано для того, что бы отсечь данный код в эдиторе при выбранной платформе андроид
    {
      touchPosition = Camera.main.ScreenToWorldPoint(new Vector3(Input.GetTouch(0).position.x, Input.GetTouch(0).position.y, Camera.main.transform.position.y));
    }
    #endif
    #if UNITY_EDITOR
    touchPosition = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, Camera.main.transform.position.y));
    #endif

    if(touchPosition.z < _touchStartPosition.z && _touchStartPosition.z - touchPosition.z <= maxShiftDistance)
    {
      _pistonDistance = _touchStartPosition.z - touchPosition.z;
      _pistone.position = new Vector3(_pistone.position.x, _pistone.position.y, _startPosition.z - (_touchStartPosition.z - touchPosition.z));
    }
  }

  private void DoPower()
  {
    _pistone.position = _startPosition;
    if(_pistonDistance < minShiftDistance)
    {
      _pistonDistance = 0;
    }
    else if(_pistonDistance > maxShiftDistance)
    {
      _pistonDistance = maxShiftDistance;
    }

    if(_pistonDistance > 0)
    {
      if(pistonTrigger != null)
      {
        pistonTrigger.CickBall(_pistonDistance*100/maxShiftDistance);
      }
      else
      {
        Debug.LogError(string.Format("[{0}][DoPower]pistonTrigger is empty!", GetType().Name));
      }
    }
  }
}