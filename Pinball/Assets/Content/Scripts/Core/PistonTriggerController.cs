﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PistonTriggerController: MonoBehaviour
{
  public float minForce = 100.0f;
  public float maxForce = 100.0f;

  private List<Rigidbody> ballLit = new List<Rigidbody>();

  private void OnEnable()
  {
    EventHolder.Instance.aiCickBallEvent += CickBall;
  }

  public void CickBall(float powerPercent)
  {
    foreach(Rigidbody ball in ballLit)
    {
      float force = (maxForce - minForce) * powerPercent / 100;
      ball.AddForce(Vector3.forward * (minForce + force));
    }
  }

  private void OnTriggerEnter(Collider colliderObject)
  {
    if(colliderObject.attachedRigidbody)
    {
      ballLit.Add(colliderObject.attachedRigidbody);
    }
  }

  private void OnTriggerExit(Collider colliderObject)
  {
    if(colliderObject.attachedRigidbody)
    {
      ballLit.Remove(colliderObject.attachedRigidbody);
    }
  }

}