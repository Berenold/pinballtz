﻿using UnityEngine;
using System.Collections;

public class AITriggerController: MonoBehaviour
{
  public ControllPosition triggerPosition;

  void OnTriggerStay(Collider colliderObject)
  {
    if(colliderObject.attachedRigidbody)
    {
      if(ApplicationContainer.Instance.aiControll && colliderObject.tag == "Ball")
      {
        EventHolder.Instance.AiHandleAction(triggerPosition);
      }
    }
  }
}
